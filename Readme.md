## Travix exercise(_by Andrei Husakou_)
---

### Environment
---

As there were no requirements for sdk and runtime, the project uses [.netcore 2.0 framework](https://www.microsoft.com/net/download/).

### Startup instructions
---

To provide consistent behaviour of build script accross different palatforms, MSBuild was chosen over Powershell or Bash script. 
So the following instructions should be the same for all platforms:

1. Ensure [dotnet 2.x sdk installed](https://www.microsoft.com/net/download/).
2. Add **_dotnet_** to Path.
3. Execute the following command from within the root directory.
```sh
dotnet msbuild WingsOn.msbuild
```
4. Navigate to build output of WingsOn.Host project
5. Run
```sh
dotnet WingsOn.Host.dll
```

### API spec
---

Swagger middleware was added and configured to keep API documentation up to date.
Run the project. Navigate to **_\*basePath\*/swagger_**

### Projects description
---

**_Domain (as per test)_**

Domain POCO entities.

**_Dal (as per test)_**

Encapsulates data store and provides repositories for data access.

_NOTES: This project was re-targeted to support DI registration. See ServiceCollectionExtensions.cs_ 

**_Host_**

Console application to provide hosting for the project, it uses [Kestrel server](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-2.1&tabs=aspnetcore2x).

**_WebAPI_**

The actual implementation of api from requirements.

**_Tests_**

Container for unit tests, using [xunit framework](https://xunit.github.io/)

_NOTE: Full code coverage was not the purpose of this test_

### Implementation notes
---

Api was designed with versioning in mind. 

Default .net core capabilities for logging, configuration and DI utilized.

Custom conventional routing system was designed and implemented. See _WingsOn.WebAPI.FrameworkCustomization.ControllersTreeRoutingConvention.cs_

### Known issues
---

Custom implementation of routes convention is not very friendly with default configuration of Swagger middleware.
Therefore operation for each _RootController(inside unique namesapace)_ are grouped under the same section _Root_.

There is a solution to name namespace root controllers the same as namespace(some refactoring of _ControllersTreeRoutingConvention.cs_ is needed).
Another solution is to play with configs of swagger midlleware.
