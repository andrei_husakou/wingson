﻿using NSubstitute;
using WingsOn.WebAPI.AppServices;
using WingsOn.WebAPI.AppServices.Contracts;
using WingsOn.WebAPI.Controllers.Customer;
using WingsOn.WebAPI.Models.Response;
using Xunit;

namespace WingsOn.Tests.Controllers
{
    public class CustomerControllerTests
    {
        public class GetCustomerMethod
        {
            private ICustomerService customerServiceSubstitute;
            private RootController customerController;

            [Fact]
            public void CustomerServiceReturnedNullCustomer_NotFoundReturned()
            {
                Initialize(null);

                var result = customerController.Get(Arg.Any<int>());
                customerController.Received().NotFound();
            }

            [Fact]
            public void CustomerServiceReturnedCustomer_OkWithPayloadReturned()
            {
                var customer = new Customer();
                Initialize(customer);

                var result = customerController.Get(Arg.Any<int>());
                customerController.Received().Ok(Arg.Is(customer));
            }

            private void Initialize(Customer customer)
            {
                customerServiceSubstitute = Substitute.For<ICustomerService>();
                customerController = Substitute.ForPartsOf<RootController>(customerServiceSubstitute);

                customerServiceSubstitute
                    .Get(Arg.Any<int>())
                    .Returns(customer);
            }
        }
    }
}
