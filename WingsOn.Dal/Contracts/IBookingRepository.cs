﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.Dal.Contracts
{
    public interface IBookingRepository : IRepository<Booking>
    {
        IEnumerable<Person> GetPassengers(Expression<Func<Booking, bool>> criteria);
    }
}
