﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.Dal.Contracts
{
    public interface IPersonRepository : IRepository<Person>
    {
    }
}
