﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WingsOn.Domain;

namespace WingsOn.Dal
{
    public interface IRepository<T> where T : DomainObject
    {
        IEnumerable<T> GetAll();

        T Get(int id);

        IEnumerable<T> FindByCriteria(Expression<Func<T, bool>> criteria);

        void Save(T element);
    }
}
