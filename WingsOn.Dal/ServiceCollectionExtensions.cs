﻿using Microsoft.Extensions.DependencyInjection;
using WingsOn.Dal.Contracts;

namespace WingsOn.Dal
{
    public static class ServiceCollectionExtensions
    {
        public static void AddWingsOnRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IBookingRepository, BookingRepository>();
            serviceCollection.AddScoped<IPersonRepository, PersonRepository>();
        }
    }
}
