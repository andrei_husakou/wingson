﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.WebAPI.AppServices;
using WingsOn.WebAPI.FrameworkCustomization;
using WingsOn.WebAPI.Utils;
using WingsOn.Dal;
using WingsOn.WebAPI.AppServices.Contracts;

namespace WingsOn.WebAPI
{
    public static class StartupExtensions
    {
        public static void AddWingsOnApi(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IFlightInfoService, FlightInfoService>();
            serviceCollection.AddScoped<ICustomerService, CustomerService>();

            VersionProvider versionProvider = new VersionProvider();
            serviceCollection.AddSingleton(versionProvider);

            serviceCollection.AddSwaggerGen(options => {
                foreach (string version in versionProvider.GetAllVersions())
                    options.SwaggerDoc(version, new Swashbuckle.AspNetCore.Swagger.Info() { Title = Constants.APP_NAME, Version = version });

                options.DocInclusionPredicate((docName, api) => {
                    string apiPrefix = versionProvider.GetApiVersionedBasePath(docName);
                    return api.RelativePath.StartsWith(apiPrefix);
                });
            });

            LanguageUtil languageUtil = new LanguageUtil();
            serviceCollection.AddSingleton(languageUtil);

            serviceCollection.AddMvc(options => {
                options.Conventions.Add(new ControllersTreeRoutingConvention(languageUtil, versionProvider));
            });

            serviceCollection.AddWingsOnRepositories();
        }

        public static void UseWingsOnApi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(setup => {
                var versionProvider = app.ApplicationServices.GetRequiredService<VersionProvider>();
                foreach (string version in versionProvider.GetAllVersions())
                    setup.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"{Constants.APP_NAME} {version.ToUpperInvariant()}");
            });
            app.UseMvc();
        }
    }
}
