﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.Dal.Contracts;
using WingsOn.Domain;
using WingsOn.WebAPI.AppServices.Contracts;
using WingsOn.WebAPI.Models.Response;

namespace WingsOn.WebAPI.AppServices
{
    public class FlightInfoService : IFlightInfoService
    {
        private readonly IBookingRepository bookingRepository;

        public FlightInfoService(IBookingRepository bookingRepository)
        {
            this.bookingRepository = bookingRepository;
        }

        public IEnumerable<Passenger> GetFlightPassengers(string flightNumber)
        {
            IEnumerable<Person> persons = bookingRepository
                .GetPassengers(booking => booking.Flight.Number == flightNumber);

            return persons.Select(person => Passenger.Create(person, flightNumber));
        }
    }
}
