﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.WebAPI.Models.Response;

namespace WingsOn.WebAPI.AppServices.Contracts
{
    public interface IFlightInfoService
    {
        IEnumerable<Passenger> GetFlightPassengers(string flightNumber);
    }
}
