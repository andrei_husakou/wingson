﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.WebAPI.Models.Request.Filters;
using WingsOn.WebAPI.Models.Response;

namespace WingsOn.WebAPI.AppServices.Contracts
{
    public interface ICustomerService
    {
        Customer Get(int id);

        IEnumerable<Customer> GetCustomers(CustomerFilter customerFilter);
    }
}
