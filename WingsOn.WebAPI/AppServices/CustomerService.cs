﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.Dal.Contracts;
using WingsOn.Domain;
using WingsOn.WebAPI.AppServices.Contracts;
using WingsOn.WebAPI.Models.Request.Filters;
using WingsOn.WebAPI.Models.Response;

namespace WingsOn.WebAPI.AppServices
{
    public class CustomerService : ICustomerService
    {
        private readonly IPersonRepository personRepository;

        public CustomerService(IPersonRepository personRepository)
        {
            this.personRepository = personRepository;
        }

        public Customer Get(int id)
        {
            Person person = personRepository.Get(id);
            Customer customer = null;
            if(person != null) 
                customer = Customer.Create(person);

            return customer;

        }

        public IEnumerable<Customer> GetCustomers(CustomerFilter customerFilter)
        {
            IEnumerable<Person> persons = personRepository.FindByCriteria(p => customerFilter.Gender == null || p.Gender == customerFilter.Gender);

            return persons.Select(p => Customer.Create(p));
        }
    }
}
