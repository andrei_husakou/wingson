﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WingsOn.WebAPI.Utils
{
    internal class LanguageUtil
    {
        public string Pluralize(string word)
        {
            // No crazy logic to address all scenarios for this test
            return $"{word}s";
        }
    }
}
