﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WingsOn.WebAPI.FrameworkCustomization;

namespace WingsOn.WebAPI.Utils
{
    internal class VersionProvider
    {
        public const string DEFAULT_VERION = "v1";
        public const string BASE_API_VERSIONED_ROUTE_TEMPLATE = "api/{0}";

        public string GetApiVersionedBasePath(Func<IEnumerable<object>> getAttributes)
        {
            ApiVersionAttribute apiVersionAttr = getAttributes().OfType<ApiVersionAttribute>().FirstOrDefault(attr => attr != null);
            string version = apiVersionAttr != null ? apiVersionAttr.Version : DEFAULT_VERION;

            return GetApiVersionedBasePath(version);
        }

        public string GetApiVersionedBasePath(string version) => String.Format(BASE_API_VERSIONED_ROUTE_TEMPLATE, version);

        public IEnumerable<string> GetAllVersions() => new[] { DEFAULT_VERION };
    }
}
