﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.WebAPI.Models.Request.Filters
{
    public class CustomerFilter
    {
        public GenderType? Gender { get; set; }
    }
}
