﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.WebAPI.Models.Response
{
    public class Passenger
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string FlightNumber { get; set; }

        public static Passenger Create(Person person, string flightNumber)
        {
            return new Passenger {
                Id = person.Id,
                Name = person.Name,
                Email = person.Email,
                FlightNumber = flightNumber
            };
        }
    }
}
