﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.WebAPI.Models.Response
{
    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime DateBirth { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public static Customer Create(Person person)
        {
            return new Customer {
                Id = person.Id,
                Name = person.Name,
                Address = person.Address,
                DateBirth = person.DateBirth,
                Email = person.Email,
                Gender = person.Gender.ToString()
            };
        }
    }
}
