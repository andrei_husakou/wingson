﻿using System;

namespace WingsOn.WebAPI.FrameworkCustomization
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    sealed class ApiVersionAttribute : Attribute
    {
        private readonly string version;

        public ApiVersionAttribute(string version)
        {
            this.version = version;
        }

        public string Version
        {
            get { return version; }
        }
    }
}
