﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using WingsOn.WebAPI.Utils;
using RootConstants = WingsOn.WebAPI.Constants;

namespace WingsOn.WebAPI.FrameworkCustomization
{
    internal class ControllersTreeRoutingConvention : IControllerModelConvention
    {
        private readonly LanguageUtil languageUtil;
        private readonly VersionProvider versionProvider;

        public ControllersTreeRoutingConvention(LanguageUtil languageUtil, VersionProvider versionProvider)
        {
            this.languageUtil = languageUtil;
            this.versionProvider = versionProvider;
        }

        public void Apply(ControllerModel controller)
        {
            var hasRouteAttributes = controller.Selectors.Any(selector => selector.AttributeRouteModel != null);
            if (hasRouteAttributes)
                return;

            // I am not going to show here that I tend to use StringBuilder to limit memory consumption :)
            // Most of the time it's not an issue, moreover this method is called once per controller
            IEnumerable<string> pathEntries = GetNamespacePathEntries(controller.ControllerType.Namespace, controller.ControllerName);

            string basePath = versionProvider.GetApiVersionedBasePath(() => controller.Attributes);

            string template = $"{basePath}/{String.Join('/', pathEntries)}";

            foreach (var selector in controller.Selectors)
            {
                selector.AttributeRouteModel = new AttributeRouteModel()
                {
                    Template = template
                };
            }
        }

        private IEnumerable<string> GetNamespacePathEntries(string controllerNamespace, string controllerName)
        {
            IEnumerable<string> pathEntries = controllerNamespace.Split('.')
                .SkipWhile(namespaceEntry => namespaceEntry != Constants.CONTROLLERS_BASE_NAMESPACE)
                .Skip(1);

            pathEntries = pathEntries.Select(entry => entry.ToLowerInvariant())
                .SelectMany(entry => new[] { $"{languageUtil.Pluralize(entry)}", $"{{{entry}Id}}" });

            string currentControllerPathEntry = languageUtil.Pluralize(controllerName.ToLowerInvariant());
            bool shouldRemoveLastParamEntry = controllerName.Equals(Constants.DEFAULT_SUBTREE_ROOT_CONTROLLER, StringComparison.InvariantCultureIgnoreCase);
            if (shouldRemoveLastParamEntry)
                pathEntries = pathEntries.SkipLast(1);
            else 
                pathEntries = pathEntries.Append(currentControllerPathEntry);

            return pathEntries;
        }


    }
}
