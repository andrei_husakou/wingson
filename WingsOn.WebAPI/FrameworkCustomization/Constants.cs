﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WingsOn.WebAPI.FrameworkCustomization
{
    public static class Constants
    {
        public const string CONTROLLERS_BASE_NAMESPACE = "Controllers";
        public const string DEFAULT_SUBTREE_ROOT_CONTROLLER = "Root";
    }
}
