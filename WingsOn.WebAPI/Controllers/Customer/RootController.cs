﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.WebAPI.AppServices.Contracts;
using WingsOn.WebAPI.Models.Request.Filters;
using ResponseTypes = WingsOn.WebAPI.Models.Response;

namespace WingsOn.WebAPI.Controllers.Customer
{
    public class RootController : Controller
    {
        private readonly ICustomerService customerService;

        public RootController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        [HttpGet("{customerId}")]
        public IActionResult Get(int customerId)
        {
            ResponseTypes.Customer customer = customerService.Get(customerId);
            if (customer == null)
                return NotFound();

            return Ok(customer);
        }

        [HttpGet]
        public IEnumerable<ResponseTypes.Customer> GetAll(CustomerFilter customerFilter)
        {
            return customerService.GetCustomers(customerFilter);
        }
    }
}
