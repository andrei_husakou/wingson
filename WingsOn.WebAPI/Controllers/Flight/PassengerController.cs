﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WingsOn.WebAPI.AppServices.Contracts;
using WingsOn.WebAPI.Models.Response;

namespace WingsOn.WebAPI.Controllers.Flight
{
    public class PassengerController : Controller
    {
        private readonly IFlightInfoService flightInfoService;

        public PassengerController(IFlightInfoService flightInfoService)
        {
            this.flightInfoService = flightInfoService;
        }

        [HttpGet]
        public IEnumerable<Passenger> GetAll(string flightId)
        {
            return flightInfoService.GetFlightPassengers(flightId);
        }
    }
}
